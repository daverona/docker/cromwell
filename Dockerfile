FROM openjdk:11.0.11-jre-slim-buster

ARG CROMWELL_VERSION=72
ARG DEBIAN_FRONTEND=noninteractive
ENV CROMWELL_VERSION=${CROMWELL_VERSION}
ENV LANG=C.UTF-8

RUN apt-get update \
  && apt-get install --quiet --yes --no-install-recommends \
    gosu \
    openssh-client \
    tzdata \
    wget \
  && apt-get clean && rm -rf /var/lib/apt/lists/* \
  # Install cromwell
  && wget -q -P /usr/local/java "https://github.com/broadinstitute/cromwell/releases/download/${CROMWELL_VERSION}/cromwell-${CROMWELL_VERSION}.jar" \
  && ln -sfr "/usr/local/java/cromwell-${CROMWELL_VERSION}.jar" /usr/local/java/cromwell.jar \
  # Create SSH RSA key pair and data directory
  && mkdir -p /home/cromwell/.ssh && chmod 700 /home/cromwell/.ssh \
  && ssh-keygen -t rsa -f /home/cromwell/.ssh/id_rsa -q -N "" -b 4096 \
  && cp /etc/profile /home/cromwell/.profile

# Configure miscellanea
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
EXPOSE 8000/tcp
WORKDIR /var/local

ENTRYPOINT ["/docker-entrypoint.sh"]
